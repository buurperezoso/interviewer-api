import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../../lib/commonMiddleware';
import { nanoid } from 'nanoid';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const addProfile = async (event, context) => {

    const { name, employeeID, enterpriseID } = event.body;

    const profile = {
        id: nanoid(),
        name,
        employeeID,
        enterpriseID,
    };

    try {

        await dynamodb.put({
            TableName: process.env.PROFILES_TABLE_NAME,
            Item: profile,
        }).promise();

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 201,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(profile),
    };

};

export const handler = commonMiddleware(addProfile);