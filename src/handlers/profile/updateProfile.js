import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../../lib/commonMiddleware';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const updateProfile = async (event, context) => {

    const { profileID } = event.pathParameters;
    const { name, employeeID, enterpriseID } = event.body;

    const profile = {
        id: profileID,
        name,
        employeeID,
        enterpriseID
    };

    try {
        await dynamodb.put({
            TableName: process.env.PROFILES_TABLE_NAME,
            Item: profile,
        }).promise();
    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    // const params = {
    //     TableName: process.env.PROFILES_TABLE_NAME,
    //     Key: { id: profileID },
    //     UpdateExpression: 'set name = :name, employeeID = :employeeID, enterpriseID = :enterpriseID',
    //     ExpressionAttributeValues: {
    //         ':name': name,
    //         ':employeeID': employeeID,
    //         ':enterpriseID': enterpriseID
    //     },
    //     ReturnValues: 'ALL_NEW',
    // };

    // let updatedProfile;

    // try {
    //     const result = await dynamodb.update(params).promise();
    //     updatedProfile = result.Attributes;
    // } catch (error) {
    //     throw new createError.InternalServerError(error);
    // }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(profile),
    };
};

export const handler = commonMiddleware(updateProfile);