import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../../lib/commonMiddleware';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const deleteProfile = async (event, context) => {

    const { profileID } = event.pathParameters;

    try {
        await dynamodb.delete({
            TableName: process.env.PROFILES_TABLE_NAME,
            Key: { id: profileID },
        }).promise();
    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify({ message: 'successfully removed' }),
    };

};

export const handler = commonMiddleware(deleteProfile);