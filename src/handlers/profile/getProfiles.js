import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../../lib/commonMiddleware';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const getProfiles = async (event, context) => {

    let profiles;

    try {

        const result = await dynamodb.scan({
            TableName: process.env.PROFILES_TABLE_NAME,
        }).promise();

        profiles = result.Items;

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(profiles),
    };

};

export const handler = commonMiddleware(getProfiles);