import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../lib/commonMiddleware';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const addSkills = async (event, context) => {

    const { interviewID } = event.pathParameters;
    const skills = event.body;
    let updated;

    try {

        const params = {
            TableName: process.env.INTERVIEWS_TABLE_NAME,
            Key: { id: interviewID },
            UpdateExpression: 'set skills = :skills',
            ExpressionAttributeValues: {
                ':skills': skills
            },
            ReturnValues: 'ALL_NEW',
        };

        const result = await dynamodb.update(params).promise();
        updated = result.Attributes;

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(updated.skills),
    };

};

export const handler = commonMiddleware(addSkills);