import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../lib/commonMiddleware';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const addInterview = async (event, context) => {

    const { interviewID } = event.pathParameters;
    const interviewers = event.body;
    let updated;

    try {

        const params = {
            TableName: process.env.INTERVIEWS_TABLE_NAME,
            Key: { id: interviewID },
            UpdateExpression: 'set interviewers = :interviewers',
            ExpressionAttributeValues: {
                ':interviewers': interviewers
            },
            ReturnValues: 'ALL_NEW',
        };

        const result = await dynamodb.update(params).promise();
        updated = result.Attributes;

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(updated.interviewers),
    };

};

export const handler = commonMiddleware(addInterview);