import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../lib/commonMiddleware';
import { nanoid } from 'nanoid';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const addCandidate = async (event, context) => {

    const { interviewID } = event.pathParameters;
    const { name, email, type } = event.body;
    let updated;

    const candidate = {
        id: nanoid(),
        name,
        email,
        type,
    };

    try {

        const params = {
            TableName: process.env.INTERVIEWS_TABLE_NAME,
            Key: { id: interviewID },
            UpdateExpression: 'set candidate = :candidate',
            ExpressionAttributeValues: {
                ':candidate': candidate
            },
            ReturnValues: 'ALL_NEW',
        };

        const result = await dynamodb.update(params).promise();
        updated = result.Attributes;

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(updated.candidate),
    };

};

export const handler = commonMiddleware(addCandidate);