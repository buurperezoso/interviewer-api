import AWS from 'aws-sdk';
import createError from 'http-errors';
import { nanoid } from 'nanoid';
import commonMiddleware from '../lib/commonMiddleware';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const addInterview = async (event, context) => {

    const interview = {
        id: nanoid(),
        interviewers: [],
        candidate: null,
        skills: [],
        questions: [],
        date: new Date().getTime()
    };

    try {

        await dynamodb.put({
            TableName: process.env.INTERVIEWS_TABLE_NAME,
            Item: interview,
        }).promise();

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 201,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(interview),
    };

};

export const handler = commonMiddleware(addInterview);