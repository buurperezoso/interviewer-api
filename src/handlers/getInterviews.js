import AWS from 'aws-sdk';
import createError from 'http-errors';
import commonMiddleware from '../lib/commonMiddleware';

const dynamodb = new AWS.DynamoDB.DocumentClient();

const getInterviews = async (event, context) => {

    let interviews;

    try {

        const result = await dynamodb.scan({
            TableName: process.env.INTERVIEWS_TABLE_NAME
        }).promise();

        interviews = result.Items.sort((a, b) => {
            return new Date(b.date) - new Date(a.date);
        });

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers": "Content-Type",
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE, PUT, PATCH'
        },
        body: JSON.stringify(interviews),
    };

};

export const handler = commonMiddleware(getInterviews);