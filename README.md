### Proyecto creado usando Serverless y montado en AWS.

Esta es la ruta: https://evc02tdigb.execute-api.us-west-1.amazonaws.com/dev


###### path: /get-interviews
method: GET

###### path: /add-interviewers/{interviewID}
method: POST

###### path: /add-interview
method: POST

###### path: /add-candidate/{interviewID}
method: POST

###### path: /add-skills/{interviewID}
method: POST

###### path: /add-questions/{interviewID}
method: POST

###### path: /add-comments/{interviewID}
method: POST

###### path: /delete-interview/{interviewID}
method: DELETE

###### path: /get-profiles
method: GET

###### path: /add-profile
method: POST

###### path: /delete-profile/{profileID}
method: DELETE

###### path: /update-profile/{profileID}
method: POST